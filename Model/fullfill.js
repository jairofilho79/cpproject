
function scanline(polyline_stats) {
    // console.log(polyline_stats)
    let y_min    = polyline_stats[0][0],
        y_max    = polyline_stats[0][1],
        plotable_dots = [];

    for (let p of polyline_stats) {
        if(p[0] < y_min) y_min = p[0];
        if(p[1] > y_max) y_max = p[1];
    }

    for (let y = y_min; y < y_max; y++) {
        let dots = [];
        for(let p of polyline_stats) {
            let x = p[3]*(y - p[0])+p[2];
            if(x >= 0)
                dots.push(x)
        }
        dots = dots.sort(function(a, b) {return a - b;})
        console.log(dots);
        for (let d = 0; d < dots.length; d += 2) {
            for(let x = dots[d]; x < dots[d+1]; x++) {
                plotable_dots.push([Math.round(x),Math.round(y)])
            }
        }
    }

    console.log(plotable_dots);
    return plotable_dots;
}

function scanline_stats_gen(polyline_dots){
    let stats = [];
    let dots = polyline_dots;
    for(let i = 0; i < dots.length - 1; i++) {let all_result = bresenhamLine(dots.slice(i,i+2),true); stats.push(all_result[1]);}
    return stats;
}

// function floodfill(dot,color,edgecolor) {
//     [x,y] = dot;
//
//     if(framebuffer[x] === undefined) { framebuffer[x] = {};}
//     framebuffer[x][y] = color;
//     if(x+1 < width ) {if(framebuffer[x+1] === undefined) { framebuffer[x+1] = {};} if(framebuffer[x+1][y] != edgecolor && framebuffer[x+1][y] != color) {floodfill([x+1,y],color,edgecolor);} }
//     if(y+1 < height) {if(framebuffer[x  ] === undefined) { framebuffer[x] = {};}   if(framebuffer[x][y+1] != edgecolor && framebuffer[x][y+1] != color) {floodfill([x,y+1],color,edgecolor);} }
//     if(x > 0       ) {if(framebuffer[x-1] === undefined) { framebuffer[x-1] = {};} if(framebuffer[x-1][y] != edgecolor && framebuffer[x-1][y] != color) {floodfill([x-1,y],color,edgecolor);} }
//     if(y > 0       ) {if(framebuffer[x  ] === undefined) { framebuffer[x] = {};}   if(framebuffer[x][y-1] != edgecolor && framebuffer[x][y-1] != color) {floodfill([x,y-1],color,edgecolor);} }
//
//
// }

function floodfill(dot,color,edgecolor,dotsLists,dotsIndexes) {
    // if(color === edgecolor) return false;
    [x,y] = dot;
    if(framebuffer[x] === undefined) { framebuffer[x] = {};}
    if(framebuffer[x][y] != edgecolor && framebuffer[x][y] != color) {flood();}

    function flood() {
        framebuffer[x][y] = color;
        if(findArr([x+1,y],dotsLists[0]) === false && x+1 < width ) dotsLists[0].push([x+1,y]);
        if(findArr([x,y+1],dotsLists[1]) === false && y+1 < height) dotsLists[1].push([x,y+1]);
        if(findArr([x-1,y],dotsLists[2]) === false && x > 0     ) dotsLists[2].push([x-1,y]);
        if(findArr([x,y-1],dotsLists[3]) === false && y > 0     ) dotsLists[3].push([x,y-1]);
        if(x < width  && dotsLists[0].length > dotsIndexes[0]+1) {dotsIndexes[0]++; floodfill(dotsLists[0][dotsIndexes[0]],color,edgecolor,dotsLists,dotsIndexes); }
        if(y < height && dotsLists[1].length > dotsIndexes[1]+1) {dotsIndexes[1]++; floodfill(dotsLists[1][dotsIndexes[1]],color,edgecolor,dotsLists,dotsIndexes); }
        if(x > 0      && dotsLists[2].length > dotsIndexes[2]+1) {dotsIndexes[2]++; floodfill(dotsLists[2][dotsIndexes[2]],color,edgecolor,dotsLists,dotsIndexes); }
        if(y > 0      && dotsLists[3].length > dotsIndexes[3]+1) {dotsIndexes[3]++; floodfill(dotsLists[3][dotsIndexes[3]],color,edgecolor,dotsLists,dotsIndexes); }
    }
    function findArr(dot,arr) {
        for (let d in arr) {
            if(JSON.stringify(dot) === JSON.stringify(arr[d])) return Number(d);
        }
        return false;
    }
}

function getLinesConfig(points) {
    //generate line
    let lines = [];
    for (let i = 1; i < points.length; i++) {
        lines.push(new Line(points[i - 1], points[i]));
    }

    // find min and max
    let minY = points[0].y;
    let maxY = points[0].y;
    for (let i = 1; i < points.length; i++) {
        let temp = points[i].y;
        if      (temp < minY) minY = temp;
        else if (temp > maxY) maxY = temp;
    }
    return [lines,minY,maxY];
}

function Line(start, end) {
    this.x0 = start.x;
    this.x1 = end.x;
    this.y0 = start.y;
    this.y1 = end.y;
    this.m = (this.y1 - this.y0) / (this.x1 - this.x0);

    this.getX = function (y) {
        if (!this.isValidY(y))
            throw new RangeError();

        return ((1 / this.m) * (y - this.y0) + this.x0);
    }

    this.isValidY = function (y) {
        if (y >= this.y0 && y < this.y1) {
            return true;
        }
        if (y >= this.y1 && y < this.y0) {
            return true;
        }

        return false;
    }
}

//draw fill line
function drawFillLine(lines,minY,maxY,condition) {
    let dots = [[],[]];
    for (let y = minY; y < maxY; y++) {
        let meetPoint = getMeetPoint(lines, y);
        for (let x = 1; x < meetPoint.length; x += 2) {
            dots[0].push([Math.round(meetPoint[x - 1]), y], [Math.round(meetPoint[x]), y]);
            dots[1] = dots[1].concat(bresenhamLine([[Math.round(meetPoint[x - 1]), y], [Math.round(meetPoint[x]), y]]));
        }
    }
    return dots;
}

function drawFillCircle(r,center) {

}

function getMeetPoint(lines,y) {
    let meet = [];
    for (let i = 0; i < lines.length; i++) {
        let l = lines[i];
        if (l.isValidY(y)) {
            meet.push(l.getX(y));
        }
    }

    // sort
    for (let i = 0; i < meet.length; i++) {
        for (let j = i; j < meet.length; j++) {
            if (meet[i] > meet[j]) {
                let temp = meet[i];
                meet[i] = meet[j];
                meet[j] = temp;
            }
        }
    }

    return  meet;

}

exports = {scanline}
