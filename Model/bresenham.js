function reflection(m,dots) {
    if(!Array.isArray(dots)) return;
    let x1,x2,y1,y2;
    [[x1,y1],[x2,y2]] = dots;
    //              x     y     xy
    let change = [false,false,false];


    if(m>1 || m<-1) {
        let temp = x1;
        x1 = y1
        y1 = temp;

        temp = x2
        x2 = y2
        y2 = temp;

        change[2] = true;
    }

    if(x1 > x2) {
        x1 *=-1
        x2 *=-1
        change[0] = true;
    }

    if(y1 > y2) {
        y1 *=-1
        y2 *=-1
        change[1] = true;
    }

    return [[[x1,y1],[x2,y2]],change];

}

function reverse_reflection (m,dots,change) {
    if(!Array.isArray(dots)) return;
    if(!Array.isArray(change)) return;

    let newDots = [];
    // 0 - Ymin, 1 - Ymax,  2 - Xymin, 3 - 1/m
    //                    0          1          2  3
    let scanline_stats = [99999999, -999999999, 0];

    for (let dot of dots) {
        let x,y;
        [x,y] = dot;
        if(change[1]) {y *=-1}

        if(change[0]) {x *=-1}

        if(change[2]) {let temp = x; x = y; y = temp;}

        newDots.push([x,y]);

        //stats to scanline algorithm
        if(x < scanline_stats[0]) {scanline_stats[0] = x; scanline_stats[2] = y;}
        if(x > scanline_stats[1])  scanline_stats[1] = x;

    }


    return [newDots,scanline_stats];
}

function bresenhamLine(dots,scanline=false) {
    if(!Array.isArray(dots)) return;
    let m = 0, e = 0;
    let x1,x2,y1,y2;
    let t = [];
    if(dots[1][1] < dots[0][1]) {t = dots[0]; dots[0] = dots[1]; dots[1] = t;} //Desenha igual independente da ordem. Começa sempre do menor y.
    [x1,y1] = Object.values(dots[0]);
    [x2,y2] = Object.values(dots[1]);
    let change = [];

    m = (y2-y1)/(x2-x1);
    let scanline_m = Math.abs(m) === Infinity || m == -0 ? 0 : m;

    //TODO: Avisar que a reflexão não pode ser o primeiro passo, visto que ela depende do m.
    [dots,change] = reflection(m,dots);

    [[x1,y1],[x2,y2]] = dots;

    let x = x1, y = y1;
    m = (y2-y1)/(x2-x1);
    e = m - 0,5;

    let tempdots = [[x,y],[x2,y2]];

    while(x < x2-1) {
        if (e > 0) {y += 1; e -= 1;}
        x += 1; e += m;

        tempdots.push([x, y]);
    }

    let result = reverse_reflection(m,tempdots,change)
    if(scanline == true) {
        result[1][3] = scanline_m;
        return result;
    }
    return result[0];
}

function bresenhamCircle(r,center) {
    if(!Array.isArray(center)) return;

    let x = 0,
        y = r,
        p = 1 - r,
        dots = [];

    dots.push([x,y]);

    while(x<y) {
        x++;
        if(p<0) {
            p += 2*x + 3;
        } else {
            y--;
            p += 2*x - 2*y + 5;
        }
        dots.push([x,y]);
    }
    //Move to original center and reflecting the dots.
    let alldots = [];
    //dx = deslocamento de x, dy = deslocamento de y. Adicionei r, porque se alguma coordenada for menor que 0, o canvas coloca o pixel em ordem inversa (tipo no python lista[-1] pega o último)
    let dx = Number(center[0]),
        dy = Number(center[1]);
    for(let d in dots) {
        [x,y] = dots[d];

        alldots.push([x  + dx, y + dy])
        alldots.push([-x + dx, y + dy])
        alldots.push([-y + dx, x + dy])
        alldots.push([-y + dx,-x + dy])
        alldots.push([-x + dx,-y + dy])
        alldots.push([x  + dx,-y + dy])
        alldots.push([y  + dx,-x + dy])
        alldots.push([y  + dx, x + dy])
    }
    return alldots;
}

function bresenhamLog(dots) {
    if(!Array.isArray(dots)) return;
    console.log(dots)

    let m = 0, e = 0;
    [x1,y1] = Object.values(dots[0]);
    [x2,y2] = Object.values(dots[1])
    let change = [];

    m = (y2-y1)/(x2-x1);

    //TODO: Avisar que a reflexão não pode ser o primeiro passo, visto que ela depende do m.
    [dots,change] = reflection(m,dots);

    [[x1,y1],[x2,y2]] = dots;

    let x = x1, y = y1;
    m = (y2-y1)/(x2-x1);

    e = m - 0,5;

    let tempdots = [[x,y]]
    console.log(m);
    while(x < x2) {
        if (e > 0)/* only > 0*/ {y += 1; e -= 1;}
        x += 1; e += m;
        console.log(x,y,e);
        tempdots.push([x, y]);
    }
    console.log(tempdots);
    return reverse_reflection(m,tempdots,change)
}

exports = { bresenhamLine, bresenhamCircle, bresenhamLog};