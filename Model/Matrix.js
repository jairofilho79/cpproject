function d2r(grau) {return grau * Math.PI/180}
const cos = Math.cos;
const sen = Math.sin;

function transposta(m) {
    console.log(m);
    let m2 = [];
    for(let x = 0; x < m[0].length; x++) {m2.push([]); for(let y = 0; y < m.length; y++) {m2[x].push(0);}}
    for(let i in m) {
        for(let j in m[i]) {
            m2[j][i] = m[i][j];
        }
    }
    return m2;
}

function multMatrix(m1,m2) {
    let matrix = [];
    for (let i = 0; i < m2.length; i++) {
        matrix.push([]);
        if(m2.length !== m1[i].length) return "Número de linhas e colunas das matrizes são diferentes.";
        for(let j = 0; j < m2[i].length; j++) {
            matrix[i].push(0);
            for (let k = 0; k < m1[i].length; k++) {

                matrix[i][j] += Number(m1[i][k]) * Number(m2[k][j]);
            }
        }
    }
    return matrix;
}

function rotateX(graus) {
    let o = d2r(graus);
    return [
        [1,  0   ,   0   ,0],
        [0,cos(o),-sen(o),0],
        [0,sen(o), cos(o),0],
        [0,   0  ,   0   ,1]
    ]
}

function rotateY(graus) {
    let o = d2r(graus);
    return [
        [cos(o) ,0,sen(o),0],
        [   0   ,1,   1  ,0],
        [-sen(o),0,cos(o),0],
        [   0   ,0,   0  ,1]
    ]
}

function rotateZ(graus) {
    let o = d2r(graus);
    return [
        [cos(o),-sen(o),0,0],
        [sen(o), cos(o),0,0],
        [   0  ,   0   ,1,0],
        [   0  ,   0   ,1,1]
    ]
}

function translate(eixo,d) {
    let dx,dy,dz;
    switch (eixo) {
        case "X": { dx = d; dy = 0; dz = 0; break;}
        case "Y": { dx = 0; dy = d; dz = 0; break;}
        case "Z": { dx = 0; dy = 0; dz = d; break;}
    }
    return [
        [1,0,0,dx],
        [0,1,0,dy],
        [0,0,1,dz],
        [0,0,0, 1]
    ]
}

function scale(eixo,s) {
    let sx,sy,sz;
    switch (eixo) {
        case "X": sx = s; sy = 1; sz = 1; break;
        case "Y": sx = 1; sy = s; sz = 1; break;
        case "Z": sx = 1; sy = 1; sz = s; break;
    }
    return [
        [sx,0,0,0],
        [0,sy,0,0],
        [0,0,sz,0],
        [0,0,0 ,1]
    ]
}

//Projeções

function XY_Plan(m,tz) {
    let mat = transposta(m);
    mat = multMatrix([
        [1,0,0,0],
        [0,1,0,0],
        [0,0,0,tz],
        [0,0,0,1]
    ],mat);
    mat.splice(2,2);
    return transposta(mat);
}

function YZ_Plan(m,tx) {
    let mat = transposta(m);
    mat = multMatrix([
        [0,0,0,tx],
        [0,1,0,0],
        [0,0,1,0],
        [0,0,0,1]
    ],mat);
    mat.splice(3,1)
    mat.splice(0,1);
    return transposta(mat);
}

function XZ_Plan(m,ty) {
    let mat = transposta(m);
    mat = multMatrix([
        [1,0,0,0],
        [0,0,0,ty],
        [0,0,1,0],
        [0,0,0,1]
    ],mat);
    mat.splice(3,1)
    mat.splice(1,1);
    return transposta(mat);
}

function obliqueProjection(m,grau,g) {
    let o = d2r(grau);

    let mat = transposta(m);
    mat = multMatrix([
        [1,0,g*cos(o),0],
        [0,1,g*sen(o),0],
        [0,0,0       ,0],
        [0,0,0       ,1]
    ],mat);
    mat.splice(2,2);
    return transposta(mat);
}

function perpectiveProjection(m,d) {

    let mat = transposta(m);
    mat = multMatrix([
                    [d,0,0,0],
                    [0,d,0,0],
                    [0,0,d,0],
                    [0,0,1,0]
                    ],mat);

    mat = transposta(mat);
    
    for (let i in mat) {
        for(let j in mat[i]) {
            mat[i][j] = Math.round(mat[i][j]/mat[i][2]);
        }
    }
        
    mat.splice(2,2);
    
    return mat;
}
