class Dot {
    constructor(x,y,z=0,color=undefined) {
        this.x = Math.round(x);
        this.y = Math.round(y);
        this.z = Math.round(z);
        this.h = 1;
        this.color = color ? color : '#000';
    }
    getCoordinates() {return [this.x,this.y,this.z,this.h];}

    getObjDot() {return {'x' : this.x, 'y': this.y, 'z': this.z, 'h': this.h, 'color': this.color}}

    getColor() {return this.color;}

    setColor(color) {this.color = color; return color;}
}

class Polyline {
    constructor(dots,id,name='') {
        this.id = id;
        this.name = name;
        this.dots = [];
        this.type = 'linha';
        this.edgeColor = '#000';
        if(!Array.isArray(dots)) {if (this.dots[dots.x] == undefined) {this.dots.push(dots)}}
        else {this.dots = dots}
    }
    getAllCoordinatesDots() {
        let dots = [];
        for(let dot of this.dots) {dots.push(dot.getCoordinates());}
        return dots;
    }
    getAllObjDots() {
        let dots = [];
        for(let dot of this.dots) {dots.push(dot.getObjDot());}
        return dots;
    }
    isMyDot(x,y) {
        for(let d in this.dots) {
            if(dots[d].x == x && dots[d].y == y) {
                return Number(d);
            }
        }
        return false;
    }
    getSmallerDot() {
        let dot, h = 99999999999999;
        for(let d in this.dots) {
            let newH = (this.dots[d].x**2 + this.dots[d].y**2)**(1/2);
            if(newH < h) {
                dot = this.dots[d];
                h = newH;
            }
        }
        return dot;
    }
    addDot(dot)      {this.dots.push(dot);}
    addDots(new_dots){
        for (var i = 0; i < new_dots.length; i++) {
            this.addDots(new_dots[i]);
        }
    }
    removeDot(dot)   {this.dots.pop(dot);}
    removeAllDots(){
        for (var i = 0; i < dots.length; i++) {
            removeDot(dot[i]);
        }
    }
    changeName(name) {this.name = name;}
}

exports = {Dot,Polyline};