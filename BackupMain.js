
//https://stackoverflow.com/questions/7812514/drawing-a-dot-on-html5-canvas

(()=>{

    // drawPixel(10, 10, 0, 0, 0, 255);
    // drawPixel(10, 20, 255, 0, 0, 255);
    // drawPixel(10, 30, 255, 0, 0, 255);
    // updateCanvas();

    // draw(bresenhamLine([[0,3],[3,9]]));
    // draw(bresenhamLine([[0,50],[20,0]]));
    // draw(bresenhamLine([[20,0],[60,0]]));
    // draw(bresenhamLine([[60,0],[90,50]]));
    // draw(bresenhamLine([[90,50],[60,90]]));
    // draw(bresenhamLine([[60,90],[20,90]]));
    // draw(bresenhamLine([[20,90],[0,50]]));

    // draw(bresenhamLine([[50,0],[100,0]]));
    // draw(bresenhamLine([[100,0],[150,100]]));
    // draw(bresenhamLine([[150,5],[100, 100]]));
    // draw(bresenhamLine([[100, 100],[50,100]]));
    // draw(bresenhamLine([[50,100],[0,50]]));

    //J
    // draw(bresenhamLine([[60,0],[60,80]]))
    // draw(bezier([[60,80],[60,120],[20,120],[20,80]]))

    //F
    // draw(bresenhamLine([[80,0],[80,110]]))
    // draw(bresenhamLine([[80,0],[100,0]]))
    // draw(bresenhamLine([[80,40],[100,40]]))

    //Circle
    // draw(bresenhamCircle(500,[100,0]));

    //Triangle
    // (y2-y1)/(x2-x1);
    // draw(bresenhamLine([[100,0],[0,100]]));
    // draw(bresenhamLine([[0,100],[200,100]]));
    // draw(bresenhamLine([[200,100],[100,0]]));
    [dots1,stats1] = bresenhamLine([ [100,0]  ,[0  ,100] ],true);
    [dots2,stats2] = bresenhamLine([ [0  ,100],[200,100] ],true);
    [dots3,stats3] = bresenhamLine([ [200,100],[100,0]   ],true);

    // [dots1,stats1] = bresenhamLine([ [100,0]  ,[0  ,100] ],true);
    // [dots2,stats2] = bresenhamLine([ [0  ,100],[100,200] ],true);
    // [dots3,stats3] = bresenhamLine([ [100,200],[200,100] ],true);
    // [dots4,stats4] = bresenhamLine([ [200,100],[100,0]   ],true);

    // draw(dots1,[255,0,0,255]);
    draw(dots1);
    // draw(dots2,[255,0,255,255]);
    draw(dots2);
    // draw(dots3,[0,0,255,255]);
    draw(dots3);
    // draw(dots4,[0,0,255,255]);
    // draw(dots4);

    // draw([[100,100]],[0,0,0,255])
    // draw([[100,99]],[0,0,0,255])

    // draw(scanline([stats1,stats2,stats3,stats4]),[0,200,0,255])

    // floodfill([100,1],[255,0,0,255],[0,0,0,255])
    // floodfill([100,100],[255,0,0,255],[0,0,0,255])
    // floodfill([100,101],[255,0,0,255],[0,0,0,255])

    floodfill(300,300,{a:255,r:255,g:0,b:0},ctx,canvasWidth,canvasHeight,5);

})();
