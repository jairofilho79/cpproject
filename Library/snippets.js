function arrDot2obj(arr,color = '#000',obj = undefined) {
    let objs = obj !== undefined ? Object.assign({},obj) : {};
    for (let a of arr) {
        if(objs[a[0]] === undefined) objs[a[0]] = {}
        objs[a[0]][a[1]] = color;
    }
    return objs;
}
