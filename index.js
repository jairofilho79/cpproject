let poligonos = [new Polyline([],`pol-${0}`,'Polígono 0')], currentPoligono = 0; //Botei inicialmente porque quando eu, de cara, adicionava mais polígonos, ficava com um a menos, e bugava.
let framebuffer = [];
let color = '#000';
let floodstats = [false,'#000'];
let polCounter = 0;
//Maior que isso dá problema de overflow, vou ter que refatorar em breve.
let width = 50, height = 50;

// Variáveis da tela de recorte de linha
let recorte = new Polyline([],'Area-Recorte');
let ponto1, ponto2 = "";
let ymax, ymin, xmax, xmin = 0;
const INSIDE = 0; // 0000 
const LEFT = 1;   // 0001 
const RIGHT = 2;  // 0010 
const BOTTOM = 4; // 0100 
const TOP = 8;    // 1000

// Variáveis da tela de recorte de polígono
let recortePoligono = new Polyline([],'Polígono-Recorte');

//Váriáveis de Transformação
let transformações = [];

//Variáveis de projeção
let currentProjection = 'XY';
let currentAngleProjection = 45;

//Cubo
let cubo = new Polyline([],`pol-${polCounter}`, `Cubo`);
let cuboPerspectiva = []


//-------------- FUNÇÕES DA INTERFACE DO USUÁRIO --------------------

function adicionaPixel(i,j){
    if(floodstats[0]) {
        floodfill(
            [i,j],
            floodstats[1],
            poligonos.length !== 0 ? poligonos[currentPoligono].edgeColor : '#000',
            [[[i,j]],[[i,j]],[[i,j]],[[i,j]]],
            [0,0,0,0]
        );
        pintaPixel(framebuffer,true);
        floodstats[0] = false; return;
    }
    let z = document.getElementById('zindex').value == "" ? 0 : Number(document.getElementById('zindex').value);

    let ponto = new Dot(i,j,z,color);
    if(poligonos.length === 0) {
        poligonos.push(new Polyline(ponto,`pol-${currentPoligono}`))
    } else {
        poligonos[currentPoligono].addDot(ponto);
    }
    if(framebuffer[i] === undefined) framebuffer[i] = {};
    framebuffer[i][j] = '#A9A9A9';
    // poligonos.push(ponto);
    pintaPixel(framebuffer,true);
}

function excluiPoligono(){
    if(poligonos[currentPoligono].name == 'Cubo'){
        desenhaCubo(cubo.getAllCoordinatesDots(),'#fff');
        desenhaCubo(cuboPerspectiva,'#fff');
    }
    poligonos.splice(currentPoligono,1);
    desenhaPoligonos(); //Serve como refresh
    currentPoligono = currentPoligono === 0 ? 0 : currentPoligono-1;
    document.getElementById("lista-poligonos").options[currentPoligono].selected = true;
}

function desenhaTabela(){

    // Div na qual iremos inserir a tabela
    let area = document.getElementById('tabela-pixels');

    // Criação do elemento table
    let tabela = document.createElement('TABLE');

    // Adição do elemento table na div
    area.appendChild(tabela);

    for(let i = 0; i <height; i++) {

        // Cria as linhas, isto é, as tr
        let pixel_y = document.createElement('TR');
        tabela.appendChild(pixel_y);

        for(let j = 0; j < width; j++){

            let pixel_x = document.createElement('TD');
            let id = document.createAttribute("ID");
            let onclick = document.createAttribute("ONCLICK");
            let classe = document.createAttribute("CLASS");
            let title = document.createAttribute("TITLE");

            // Adiciona um novo elemento
            pixel_y.appendChild(pixel_x);

            // Seta os valores a serem adicionados
            id.value = j + ','+ i;
            onclick.value = 'adicionaPixel('+j+','+i+')';
            classe.value = 'pixels';
            title.value = '[' +j + ','+ i +']';

            // Adiciona os atributos no novo nó
            pixel_x.setAttributeNode(classe);
            pixel_x.setAttributeNode(id);
            pixel_x.setAttributeNode(title);
            pixel_x.setAttributeNode(onclick);


        }
    }
}

function desenhaPoligonos() {
    const lista = document.getElementById('lista-poligonos');
    lista.innerHTML = "";

    for (let p in poligonos) {
        const opcao = document.createElement('option');
        opcao.innerHTML = poligonos[p].name;

        const onclick = document.createAttribute("ONCLICK");

        onclick.value = 'alternaPoligono('+p+')';

        lista.appendChild(opcao);
        opcao.setAttributeNode(onclick);
    }
}

function novoPoligono() {
    polCounter++;
    poligonos.push(new Polyline([],`pol-${polCounter}`, `Polígono ${polCounter}`));
    desenhaPoligonos();//Serve como refresh
    currentPoligono = poligonos.length-1;
    //Serve para o usuário se guiar em qual Polígono ele está alterando.
    document.getElementById("lista-poligonos").options[currentPoligono].selected = true;

    //É interessante deixar todos os polígonos na tela, fica estranho apagar tudo quando cria ou troca de polígono.
    // limparTela();
}



function alternaPoligono(index){
    currentPoligono = index;
}

function exibePontosPoligono(){
    limparTela();
    let pontos = poligonos[currentPoligono].getAllCoordinatesDots();

    if (pontos != null){
        framebuffer = arrDot2obj(pontos,'#696969',framebuffer);
        pintaPixel(framebuffer,true);
    } else {
        alert("O polígono atual não possui pontos");
    }
}

function limparTela(){
    pintaPixel(framebuffer,false);
    framebuffer = {};
}

//-------------- ALGORITMOS DE RASTERIZAÇÃO --------------------

function getBresenhamDots(dots) {
    let new_dots = [];
    for(let i = 0; i < dots.length - 1; i++) {new_dots = new_dots.concat(bresenhamLine(dots.slice(i,i+2)))}
    return new_dots;
}

function linha(dots = projetar(poligonos[currentPoligono].getAllCoordinatesDots())){
    poligonos[currentPoligono].type = "linha";
    framebuffer = arrDot2obj(getBresenhamDots(dots),color,framebuffer);
    pintaPixel(framebuffer,true);
}

function circulo(dots = projetar(poligonos[currentPoligono].getAllCoordinatesDots().slice(-2))) {
    poligonos[currentPoligono].type = "circulo";
    try{framebuffer[dots[0][0]][dots[0][1]] = '#FFF';} catch(e) {}
    framebuffer = arrDot2obj(bresenhamCircle(Math.pow(Math.pow(dots[1][0] - dots[0][0],2) + Math.pow(dots[1][1] - dots[0][1],2),0.5),dots[0]),color,framebuffer);
    pintaPixel(framebuffer,true);
    delete framebuffer[dots[0][0]][dots[0][1]];
}

function curva(dots = projetar(poligonos[currentPoligono].getAllCoordinatesDots())) {
    poligonos[currentPoligono].type = "curva";
    if(dots.length < 4) {window.alert("Quantidade de pontos de controle insuficiente para uma curva Beziér cúbica."); return;}
    let bezierDots = [];
    for(let i = 0; i < dots.length - 3; i+=3) {bezierDots = bezierDots.concat(bezier(dots.slice(i,i+4)));}
    for(let i = 0; i < bezierDots.length - 1; i++) {framebuffer = arrDot2obj(bresenhamLine(bezierDots.slice(i,i+2)),color,framebuffer);}
    pintaPixel(framebuffer,true);
}

function preenchimentoScanLine(jscolor)  {
    if(poligonos.length === 0) {alert('Por favor, crie um polígono!'); return;}

    let temp_dots = projetar(poligonos[currentPoligono].getAllCoordinatesDots());
    let dots = [];
    temp_dots.forEach(dot => {
        dots.push(new Dot(...dot))
    })
    if (dots[0].x !== dots[dots.length-1].x || dots[0].y !== dots[dots.length-1].y) {dots.push(dots[0])}
    let scan_dots = drawFillLine(...getLinesConfig(dots)); console.log(scan_dots);
    framebuffer = arrDot2obj(scan_dots[1],`#${jscolor}`,framebuffer);
    framebuffer = arrDot2obj(scan_dots[0].concat(getBresenhamDots(projetar(poligonos[currentPoligono].getAllCoordinatesDots()))),poligonos[currentPoligono].edgeColor,framebuffer);
    pintaPixel(framebuffer,true);
}

function preenchimentoFloodFill(jscolor) {floodstats = [true,`#${jscolor}`]}

//-------------- ALGORITMOS DE PROJEÇÃO --------------------
function projetar(m) {
    let o = currentAngleProjection;
    switch(currentProjection) {
        case "XY":
            return XY_Plan(m,0);
        case "XZ":
            return XZ_Plan(m,0)
        case "YZ":
            return YZ_Plan(m,0);
        case "Cavalier":
            return obliqueProjection(m,o,1)
        case "Cabinet":
            return obliqueProjection(m,o,0.5)
    }
}
function mudarProjecao(proj) {currentProjection = proj; limparTela(); for(let p of poligonos) {eval(p.type)()}}

//-------------- ALGORITMOS DE TRANSFORMAÇÃO --------------------

function compor() {
    let eixo;
    const radios = document.getElementsByName("eixo");
    for(let i = 0; i < 3; i++) {if(radios[i].checked) {eixo = radios[i].value; break;}}
    const translateValue = document.getElementById("botao-t-t").value;
    const rotateValue    = document.getElementById("botao-t-r").value;
    const scaleValue     = document.getElementById("botao-t-s").value;
    if(translateValue != "") transformações.unshift(translate(eixo,Number(translateValue)));
    if(rotateValue    != "") transformações.unshift(eval("rotate"+eixo)(Number(rotateValue)));
    if(scaleValue     != "") transformações.unshift(scale(eixo,Number(scaleValue)));
}

function transformar() {
    compor();
    if(transformações.length === 0) return;

    let temp = transformações[0];
    for(let t = 1; t < transformações.length; t++) {temp = multMatrix(temp,transformações[t]);}
    transformações = [];

    let dots = poligonos[currentPoligono].getAllCoordinatesDots();
    let smallerDot = poligonos[currentPoligono].getSmallerDot().getCoordinates();
    for(let d in dots) {
        dots[d][0] -= smallerDot[0]
        dots[d][1] -= smallerDot[1]
    }

    dots = transposta(multMatrix(temp,transposta(dots)));

    for(let d in dots) {
        dots[d][0] += smallerDot[0]
        dots[d][1] += smallerDot[1]
    }

    let newDots = [];
    for(let d = 0; d < dots.length; d++) {newDots.push(new Dot(...dots[d],poligonos[currentPoligono].edgeColor))}
    
    let pol = new Polyline(newDots,`pol-0`, `Polígono 0`)
    limparTela();
    eval(poligonos[currentPoligono].type)(pol.getAllCoordinatesDots());
}

// ----------------- ALGORITMOS DE RECORTE ----------------------

// Recorte de Linha

function recuperaP1(){if(document.getElementById('ponto1').value != null) {ponto1 = document.getElementById('ponto1').value.split(",").map(Number); recorte.dots = recorte.dots.splice(0,recorte.dots.length);}}

function recuperaP2(){if(document.getElementById('ponto2').value != null) {ponto2 = document.getElementById('ponto2').value.split(",").map(Number); recorte.dots = recorte.dots.splice(0,recorte.dots.length);}}

function setMaxMin(){

    if(ponto1[0] > ponto2[0]){ xmax = ponto1[0]; xmin = ponto2[0]; }
    else { xmax = ponto2[0]; xmin = ponto1[0]; }

    if(ponto1[1] > ponto2[1]){ ymax = ponto1[1]; ymin = ponto2[1]; }
    else { ymax = ponto2[1]; ymin = ponto1[1]; }

}

function gerarBordaTelaRecorte() {
    let dots_recorte = [];
    let z = document.getElementById('zindex').value == "" ? 0 : Number(document.getElementById('zindex').value);
    let recorteDots = [new Dot(ponto1[0],ponto1[1],z,'#FF0000'),new Dot(ponto1[0],ponto2[1],z,'#FF0000'),new Dot(ponto2[0],ponto2[1],z,'#FF0000'),new Dot(ponto2[0],ponto1[1],z,'#FF0000'),new Dot(ponto1[0],ponto1[1],z,'#FF0000')];
    recorte = new Polyline(recorteDots,'Area-Recorte');

    let dots = projetar(recorte.getAllCoordinatesDots());

    for(let i = 0; i < dots.length - 1; i++) {dots_recorte = dots_recorte.concat(bresenhamLine(dots.slice(i,i+2)))}

    return dots_recorte;
}

function desenhaTelaRecorte() {
    setMaxMin();

    let dots_recorte = gerarBordaTelaRecorte();
    framebuffer = arrDot2obj(dots_recorte,'#FF0000', framebuffer);

    pintaPixel(framebuffer,true);

}

function codifica(x,y){

    let code = INSIDE;

    if (x < xmin)
        code |= LEFT;
    else if (x > xmax)
        code |= RIGHT;
    if (y < ymin)
        code |= BOTTOM;
    else if (y > ymax)
        code |= TOP;

    return code;
}

function recortaLinha() {
    limparTela();
    setMaxMin();
    let novospontos = [];

    for (let i = 0; i < poligonos.length; i++) {

        let pontos = poligonos[i].getAllCoordinatesDots();

        let x1 = pontos[0][0];
        let y1 = pontos[0][1];

        let x2 = pontos[1][0];
        let y2 = pontos[1][1];

        let codigo1 = codifica(x1,y1);
        let codigo2 = codifica(x2,y2);

        let accept = false;

        while(true){

            if ((codigo1 == 0) && (codigo2 == 0)){
                accept = true;
                break;
            }

            else if (codigo1 & codigo2){
                break;
            }
            else {
                let code_out;
                let x, y;

                if (codigo1 != 0)
                    code_out = codigo1;
                else
                    code_out = codigo2;

                if (code_out & TOP) {
                    x = Math.round(x1 + (x2 - x1) * (ymax - y1) / (y2 - y1));
                    y = ymax;
                }
                else if (code_out & BOTTOM){
                    x = Math.round(x1 + (x2 - x1) * (ymin - y1) / (y2 - y1));
                    y = ymin;
                }
                else if (code_out & RIGHT){
                    y = Math.round(y1 + (y2 - y1) * (xmax - x1) / (x2 - x1));
                    x = xmax;
                }
                else if (code_out & LEFT){
                    y = Math.round(y1 + (y2 - y1) * (xmin - x1) / (x2 - x1));
                    x = xmin;
                }
                if (code_out == codigo1){
                    x1 = x;
                    y1 = y;
                    codigo1 = codifica(x1, y1);
                }
                else{
                    x2 = x;
                    y2 = y;
                    codigo2 = codifica(x2, y2);
                }
            }
        }
        if (accept){ novospontos = novospontos.concat(bresenhamLine([[x1,y1],[x2,y2]]));}
    }
    console.log(novospontos);
    let borda = gerarBordaTelaRecorte();
    for (let b in borda) {borda[b] = borda[b].toString()}
    let pontos = novospontos.filter(function(val) {return borda.indexOf(val.toString()) == -1;});
    framebuffer = arrDot2obj(pontos,color,framebuffer);
    pintaPixel(framebuffer,true);

}

function recortaPoligono(){
    function xor(a,b) {return ( eval(a) || eval(b) ) && !( eval(a) && eval(b) );}

    //reta xmin
    let dots = poligonos[currentPoligono].getAllCoordinatesDots();
    dots.pop();
    let novosDots = [];

    for(let d = 0; d < dots.length; d++) {
        if(dots[d][0] >= xmin) {novosDots.push(dots[d]);}

        //verificar se está no último elemento
        if(d < dots.length-1) {
            if(xor(`${dots[d][0]} >= ${xmin}`,`${dots[d+1][0]} >= ${xmin}`)) {
                let intersection = bresenhamLine([dots[d],dots[d+1]]);
                for(let i of intersection) {if(i[0] === xmin) {novosDots.push(i); break;}}
            }
        } else {
            if(xor(`${dots[d][0]} >= ${xmin}`,`${dots[0][0]} >= ${xmin}`)) {
                let intersection = bresenhamLine([dots[d],dots[0]]);
                for(let i of intersection) {if(i[0] === xmin) {novosDots.push(i); break;}}
            }
        }
    }

    //reta ymin
    dots = [...novosDots];
    novosDots = [];

    for(let d = 0; d < dots.length; d++) {
        if(dots[d][1] >= ymin) {novosDots.push(dots[d]);}

        //verificar se está no último elemento
        if(d < dots.length-1) {
            if(xor(`${dots[d][1]} >= ${ymin}`,`${dots[d+1][1]} >= ${ymin}`)) {
                let intersection = bresenhamLine([dots[d],dots[d+1]]);
                for(let i of intersection) {if(i[1] === ymin) {novosDots.push(i); break;}}
            }
        } else {
            if(xor(`${dots[d][1]} >= ${ymin}`,`${dots[0][1]} >= ${ymin}`)) {
                let intersection = bresenhamLine([dots[d],dots[0]]);
                for(let i of intersection) {if(i[1] === ymin) {novosDots.push(i); break;}}
            }
        }
    }

    //reta xmax
    dots = [...novosDots];
    novosDots = [];

    for(let d = 0; d < dots.length; d++) {
        if(dots[d][0] <= xmax) {novosDots.push(dots[d]);}

        //verificar se está no último elemento
        if(d < dots.length-1) {
            if(xor(`${dots[d][0]} <= ${xmax}`,`${dots[d+1][0]} <= ${xmax}`)) {
                let intersection = bresenhamLine([dots[d],dots[d+1]]);
                for(let i of intersection) {if(i[0] === xmax) {novosDots.push(i); break;}}
            }
        } else {
            if(xor(`${dots[d][0]} <= ${xmax}`,`${dots[0][0]} <= ${xmax}`)) {
                let intersection = bresenhamLine([dots[d],dots[0]]);
                for(let i of intersection) {if(i[0] === xmax) {novosDots.push(i); break;}}
            }
        }
    }
    //reta ymax
    dots = [...novosDots];
    novosDots = [];

    for(let d = 0; d < dots.length; d++) {
        if(dots[d][1] <= ymax) {novosDots.push(dots[d]);}

        //verificar se está no último elemento
        if(d < dots.length-1) {
            if(xor(`${dots[d][1]} <= ${ymax}`,`${dots[d+1][1]} <= ${ymax}`)) {
                let intersection = bresenhamLine([dots[d],dots[d+1]]);
                for(let i of intersection) {if(i[1] === ymax) {novosDots.push(i); break;}}
            }
        } else {
            if(xor(`${dots[d][1]} <= ${ymax}`,`${dots[0][1]} <= ${ymax}`)) {
                let intersection = bresenhamLine([dots[d],dots[0]]);
                for(let i of intersection) {if(i[1] === ymax) {novosDots.push(i); break;}}
            }
        }
    }

    limparTela();
    novosDots.push(novosDots[0]);
    let pontos = getBresenhamDots(novosDots);
    framebuffer = arrDot2obj(pontos,poligonos[currentPoligono].edgeColor,framebuffer);
    pintaPixel(framebuffer,true);

}

function desenhaCubo(cuboDots,color){

    // Desenha o plano de trás
    framebuffer = arrDot2obj(bresenhamLine([cuboDots[0],cuboDots[1]]),color,framebuffer);
    framebuffer = arrDot2obj(bresenhamLine([cuboDots[1],cuboDots[2]]),color,framebuffer);
    framebuffer = arrDot2obj(bresenhamLine([cuboDots[2],cuboDots[3]]),color,framebuffer);
    framebuffer = arrDot2obj(bresenhamLine([cuboDots[3],cuboDots[0]]),color,framebuffer);

    // Desenha o plano da frente
    framebuffer = arrDot2obj(bresenhamLine([cuboDots[4],cuboDots[5]]),color,framebuffer);
    framebuffer = arrDot2obj(bresenhamLine([cuboDots[5],cuboDots[6]]),color,framebuffer);
    framebuffer = arrDot2obj(bresenhamLine([cuboDots[6],cuboDots[7]]),color,framebuffer);
    framebuffer = arrDot2obj(bresenhamLine([cuboDots[7],cuboDots[4]]),color,framebuffer);

    // Desenha a ligação entre os planos
    framebuffer = arrDot2obj(bresenhamLine([cuboDots[0],cuboDots[4]]),color,framebuffer);
    framebuffer = arrDot2obj(bresenhamLine([cuboDots[1],cuboDots[5]]),color,framebuffer);

    framebuffer = arrDot2obj(bresenhamLine([cuboDots[2],cuboDots[6]]),color,framebuffer);
    framebuffer = arrDot2obj(bresenhamLine([cuboDots[3],cuboDots[7]]),color,framebuffer);

    pintaPixel(framebuffer,true);

}

function geraCubo() {

    polCounter++;
    
    // let z = Number(document.getElementById('zindex'));
 
	let z = Number(document.getElementById('zindex'));

    // let cuboDots = [[0,0,1],[10,0,1],[10,10,1],[0,10,1],[0,0,1],[0,0,10],[10,0,10],[10,10,10],[0,10,10],[0,0,10],[10,0,10],[10,0,1],[10,10,1],[10,10,10],[0,10,10],[0,10,1]]
    // let porra = [];
    // for (let i of cuboDots) {
    // 	porra.push(new Dot(...i));
    // }

    // //                  0         1       2           3

    // poligonos.push(new Polyline(porra,`pol-cubo`, `Cubo`));
    // desenhaPoligonos();//Serve como refresh
    // currentPoligono = poligonos.length-1;
    // //Serve para o usuário se guiar em qual Polígono ele está alterando.
    // document.getElementById("lista-poligonos").options[currentPoligono].selected = true;

    // linha();

  
let cuboDots = [];
    
//                  0         1       2           3
    cuboDots.push([0,0],[7,0],[7,7],[0,7]);
    //          4           5       6           7
    cuboDots.push([3,3],[10,3],[10,10],[3,10]);

    for (var i = 0; i < cuboDots.length; i++) {
        let temp_point = cuboDots[i];
        cubo.addDot(new Dot(temp_point[0],temp_point[1],z,color));
    }

    poligonos.push(cubo);

    desenhaCubo(cuboDots,color);	

}


function projecaoPerspectiva(d) {

    let projecao = [
                    [d,0,0,0],
                    [0,d,0,0],
                    [0,0,d,0],
                    [0,0,1,0]
                    ];
	

    let pontos = cubo.getAllCoordinatesDots();

    // pontos = multMatrix(rotateY(45),transposta(pontos));

    // console.log(pontos)

    // pontos = transposta(pontos);

    // let dots = perpectiveProjection(pontos,d);

    // console.log(dots);

    // let dots2 = getBresenhamDots(transposta(dots));

    // console.log(dots2);

    // framebuffer = arrDot2obj(dots2,color,framebuffer);
	

    // pintaPixel(framebuffer,true);

    let result = [];

    let x,y,z,h;

    if(d != 0){

        for (var i = 0; i < pontos.length; i++) {

            x = pontos[i][0] * projecao[0][0];
            x +=  pontos[i][0] * projecao[0][1];
            x +=  pontos[i][0] * projecao[0][2];
            x +=  pontos[i][0] * projecao[0][3];

            y = pontos[i][1] * projecao[1][0];
            y +=  pontos[i][1] * projecao[1][1];
            y+=  pontos[i][1] * projecao[1][2];
            y +=  pontos[i][1] * projecao[1][3];

            z = pontos[i][2] * projecao[2][0];
            z +=  pontos[i][2] * projecao[2][1];
            z +=  pontos[i][2] * projecao[2][2];
            z +=  pontos[i][2] * projecao[2][3];

            h = pontos[i][3] * projecao[3][0];
            h +=  pontos[i][3] * projecao[3][1];
            h +=  pontos[i][3] * projecao[3][2];
            h +=  pontos[i][3] * projecao[3][3];

            result.push([x,y,z,h]);

        }

        console.log(result);

        for (let j = 0; j < pontos.length; j++){
            result[j][0] = result[j][0] / result[j][3];
            result[j][1] = result[j][1] / result[j][3];
            result[j][2] = result[j][2] / result[j][3];
            result[j][3] = result[j][3] / result[j][3];
        }
    } else {
        result = cubo.getAllCoordinatesDots();
    }

    limparTela();

    cuboPerspectiva = result;

    desenhaCubo(result,color);

}